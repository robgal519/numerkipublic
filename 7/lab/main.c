#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"



int gestosc_wykresu = 200;


double Wn(double x, int n, double *x_w, double *y_w)
{
	double w=0.0;
	double l,m;

	for(int j=0;j<=n;j++)
	{
		l=1;m=1;
		for(int i=0;i<=n;i++)
		{
			if(i!=j)
			{
				l*=(x-x_w[i]);
				m*=(x_w[j]-x_w[i]);
			}
		}
		w += y_w[j]*l/m;
	}

	return w;
}


double foo(double x)
{
	return exp(-pow(x,2));
}

void fill_vectors(double *x_w, double *y_w,double x_max,double x_min,int n)
{
double delta= (x_max-x_min)/n;
for(int i=0;i<=n;++i)
{
	//x_w[i]=x_min+delta*i;
//czybyszew
	x_w[i]=0.5*((x_max-x_min)*cos(3.1415*(2*i+1)/(2*n+2))+x_max+x_min);
	y_w[i]=foo(x_w[i]);
	printf("%f\n",delta);

}
}

int main()
{
int n=5;
double x_w[n+1];
double y_w[n+1];

double x_max = 5;
double x_min = -5;

fill_vectors( x_w , y_w , x_max , x_min , n );

double arg[gestosc_wykresu];
double fo[gestosc_wykresu];
double w_nX[gestosc_wykresu];


double deltaDuza = (x_max-x_min)/gestosc_wykresu;

FILE *file = fopen("data5c.dat","w");
fprintf(file,"arg[i]\tfoo(arg[i])\tw_n(arg[i])\n");
for(int i=0;i<=gestosc_wykresu;++i)
{
	arg[i]= x_min + deltaDuza*i;
	fo[i]=foo(arg[i]);
	w_nX[i]=Wn( arg[i] , n , x_w , y_w );
	fprintf(file,"%f\t%f\t%f\n",arg[i],fo[i],w_nX[i]);

}

fclose(file);

	return 0;
}