#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"


float foo(float x)
{
	return 1/(1+x*x);
	//return cos(2*x);
}

int gestosc = 200;
int n =20;
float x_max = 5;
float x_min = -5;

float der(float x, float delta)
{
	return (foo(x-delta)-2*foo(x)+foo(x+delta))/(delta*delta);
}

float inter(float x, float *X, float *Y, float ** M)
{
	float S=0;
	for(int i=2;i<=n;++i)
	{
		if(x >= X[i-1] && x <= X[i])
		{	
			S = M[i-1][1]*(pow((X[i]-x),3))/(6*(X[i]-X[i-1])) + M[i][1]*(pow((x-X[i-1]),3))/(6*(X[i]-X[i-1]));
			S+=( (Y[i]-Y[i-1])/(X[i]-X[i-1]) - (X[i]-X[i-1])/6 * (M[i][1] - M[i-1][1]) ) * (x - X[i-1]);
			S+= Y[i-1] - M[i-1][1] * (pow((X[i]-X[i-1]),2))/6;
			return S;
		}
	}
	return 0;
}

int main ()
{

	float *X = vector(1,n);
	float *Y = vector(1,n);

///////////////////////////////           1            //////////////
	float h = (x_max - x_min)/(n-1);
	for(int i=1;i<=n;++i)
	{
		X[i] = x_min + h*(i-1);
		Y[i] = foo(X[i]);
	}
//////////////////////////////            2         ////////////////
	float ** A = matrix(1,n,1,n);
	float ** D = matrix(1,n,1,1);
	for(int i=1; i<=n;++i)
	{
		for(int j=1;j<=n;++j)
			A[i][j] =0;
	}

	for(int i=2;i<n;++i)
	{
		A[i][i] = 2;
		A[i][i+1] = (X[i+1]-X[i])/(X[i]-X[i-1]+X[i+1]-X[i]);
		A[i][i-1] = 1 - A[i][i+1]; 
	}
	A[1][1] = 1;
	A[n][n] = 1;

	for(int i=2;i<=n;++i)
	{
		D[i][1]=(6/(X[i]-X[i-1]+X[i+1]-X[i]))*((Y[i+1]-Y[i])/(X[i+1]-X[i]) - (Y[i]-Y[i-1])/(X[i]-X[i-1]));
	}
	D[1][1] = 0;
	D[n][1] = 0;

	gaussj(A,n,D,1);
	/////////////////////////////////// 4 /////////////////////////////
float * poch = vector(1,n);
FILE * pochodneData = fopen("pochodne.dat","w");
for(int i=1;i<=n;++i)
{
	poch[i]=der(X[i], 0.01);
	fprintf(pochodneData,"%f\t%f\t%f\n",X[i],D[i][1],poch[i]);
}
fclose(pochodneData);


 float delta = (x_max - x_min)/(float)gestosc;
 float arg = x_min;
 FILE *file = fopen("data5F2.dat","w"); 
 fprintf(file,"x\ty\ty_inter\n");
	for(int i=0;i<gestosc-1;++i)
	{
		arg += delta;
		float wartY = inter(arg,X,Y,D);
		float wartY_oryg = foo(arg);
		fprintf(file,"%f\t%f\t%f\n",arg,wartY_oryg,wartY);

	}
	fclose(file);
	return 0;



}