#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"
#include <stdlib.h>

float f(float x, float y){
	return 5.0/2 * pow((x*x-y),2) + pow((1-x),2);
}

float pochodnaX(float x, float y, float  delta){
	return (f(x+delta,y) - f(x-delta,y)) / (2*delta);
}

float pochodnaY(float x, float y, float  delta){
	return (f(x,y+delta) - f(x,y-delta)) / (2*delta);
}

float norma(float x, float y){
	return sqrt(x*x + y*y);
}

int main(){

float dr = 1000.0; // inicjalizuje wartoscia ktora w pierwszej uiteracji nie zatrzyma petli;

float eps = 1e-2;

float r[2] = {-0.75,1.75};

float h = 0.1;

float delta = 1e-4;


char nazwaPliku[] = "eps1.dat";
FILE* plik = fopen(nazwaPliku,"w");
fprintf(plik,"iteracja\tnorma\tX\tY\n");
fprintf(plik,"%d\t%f\t%f\t%f\t\n",0,0.0,r[0],r[1]);

for(int i=1; dr > eps && i<=1000; ++i){

	float r_old[2]={r[0],r[1]};

	float delta_r[2] = {
		h * pochodnaX(r_old[0],r_old[1],delta),
	 	h * pochodnaY(r_old[0],r_old[1],delta)
	};

	r[0] = r_old[0] - delta_r[0];
	r[1] = r_old[1] - delta_r[1];

	dr = norma (delta_r[0], delta_r[1]);
	//dr = sqrt(pow(r[0]-r_old[0],2)+ pow(r[1]-r_old[1],2));

	fprintf(plik,"%d\t%f\t%f\t%f\t\n",i,dr,r[0],r[1]);
}

fclose(plik);

FILE * fxy = fopen("fxy.dat","w");

for(float x = -2.0; x < 2.0; x+=0.02 ){
	for(float y = -2.0; y < 2.0 ; y+=0.02){
		fprintf(fxy,"%f\t%f\t%f\n",x,y,f(x,y));
	}
	fprintf(fxy,"\n");
}

fclose(fxy);

	return 0;
}