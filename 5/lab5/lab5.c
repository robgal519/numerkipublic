#include "../numerical_recipes.c/nr.h"
#include "../numerical_recipes.c/nrutil.h"
#include "../numerical_recipes.c/nrutil.c"
#include "../numerical_recipes.c/gaussj.c"
#include "../numerical_recipes.c/tqli.c"
#include "../numerical_recipes.c/tred2.c"
#include "../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"

int main()
{
	unsigned size=7;
	float ** A;
	A = matrix(1,size,1,size);


//fill A

for(int i=1;i<=size;++i)
{
	for(int j=1;j<=size;++j)
	{
		A[i][j]=sqrt(i+j);
	}
}


float *d = vector(1,size);
float *e = vector ( 1, size);

float ** P=matrix(1,size,1,size);
for(int i=1;i<=size;++i)
{
	for(int j=1;j<=size;++j)
	{
		P[i][j]=A[i][j];
	}
}

tred2(P,size,d,e); // A is now P

float **Y = matrix(1,size,1,size);

//fill Y as diagonal marix

for(int i=1;i<=size;++i)
{
	for(int j=1;j<=size;++j)
	{
		if(i==j)
			Y[i][j]=1;		
		else
			Y[i][j]=0;
	}
}



tqli(d,e,size,Y);

//d is now \lambda


float **X = matrix(1,size,1,size);


for(int i=1;i<=size;++i)
{
	for(int k=1;k<=size;++k)
	{
		X[i][k]=0.0;
		for(int j=1;j<=size;++j)
		{
			X[i][k] += P[i][j] * Y[j][k];
		}
	}
}


float **Ax = matrix(1,size,1,size);
//
for(int i=1;i<=size;++i)
{
	for(int k=1;k<=size;++k)
	{
		Ax[i][k]=0.0;
		for(int j=1;j<=size;++j)
		{
			Ax[i][k] += A[i][j] * X[j][k];
		}
	}
}
//

FILE *output=fopen("X.dat","w");

for(int i=1;i<=size;i++){
	for(int j=1;j<=size;++j)
	{
		fprintf(output,"%f\t",X[i][j]);
	}
	fprintf(output,"\n");

}

fclose(output);


FILE *output2=fopen("lambda.dat","w");

for(int i=1;i<=size;i++){
	fprintf(output2,"%.20f \n",d[i]);

}

fclose(output2);



// float* b = vector(1,size);

// for(int i=1;i<=size;++i)
// {
// 	b[i]=0;
// 	for(int j=1;j<=size;++j)
// 	{
// 		b[i]+=X[j][i]*Ax[j][i];
// 	}
// }


float *x = vector(1,size);


for(int k=1;k<=size;++k)
{
	for(int i=1;i<size;++i)
		{
			x[i]=1.0;
		}

	double lambda;


	for(int it=1;it<=8;++it)
	{
		float *xn = vector(1,size);
		//X_n = A*x
		for(int i=1;i<=size;++i)
		{
			xn[i]=0;
			for(int j=1;j<=size;++j)
			{
				xn[i]+=A[j][i]*x[j];
			}
		}

		//l = x^T * X_n
		double l=0;
		double m=0;
		for(int i=1;i<=size;++i)
			l+=x[i]*xn[i];

		for(int i=1;i<=size;++i)
			m+=x[i]*x[i];

		lambda= l/m;
		//x_n = x_n/||x_n||
		
		double norma=0.0;
		for(int i=1;i<=size;++i)
		{
			norma += xn[i]*xn[i];
		}
		norma = sqrt(norma);


		for(int i=1;i<=size;++i)
		{
			xn[i]=xn[i]/norma;
		}
		free(x);
		x=xn;
		printf("%d\t%d\t%f\t%f\t%.20f\n", k,it,lambda,l,m);
	}

	float **XXT = matrix(1,size,1,size);
	for(int i=1;i<=size;++i)
	{
		for(int j=1;j<=size;++j)
		{
			XXT[i][j]=x[i]*x[j]*lambda;
		}
	}
	for(int i=1;i<=size;++i)
	{
		for(int j=1;j<=size;++j)
		{
			A[i][j]=A[i][j]-XXT[i][j];
		}
	}


}

return 1;
}