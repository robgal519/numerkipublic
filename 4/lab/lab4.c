
#include "../numerical_recipes.c/nr.h"
#include "../numerical_recipes.c/nrutil.h"
#include "../numerical_recipes.c/nrutil.c"
#include "../numerical_recipes.c/gaussj.c"
#include "../numerical_recipes.c/tqli.c"
#include "../numerical_recipes.c/tred2.c"
#include "../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"

int main()
{
	unsigned size=5;
	float ** A;
	A = matrix(1,size,1,size);


//fill A

for(int i=1;i<=size;++i)
{
	for(int j=1;j<=size;++j)
	{
		A[i][j]=sqrt(i+j);
	}
}


float *d = vector(1,size);
float *e = vector ( 1, size);

float ** P=matrix(1,size,1,size);
for(int i=1;i<=size;++i)
{
	for(int j=1;j<=size;++j)
	{
		P[i][j]=A[i][j];
	}
}

tred2(P,size,d,e); // A is now P

float **Y = matrix(1,size,1,size);

//fill Y as diagonal marix

for(int i=1;i<=size;++i)
{
	for(int j=1;j<=size;++j)
	{
		if(i==j)
			Y[i][j]=1;		
		else
			Y[i][j]=0;
	}
}



tqli(d,e,size,Y);

//d is now \lambda


float **X = matrix(1,size,1,size);


for(int i=1;i<=size;++i)
{
	for(int k=1;k<=size;++k)
	{
		X[i][k]=0.0;
		for(int j=1;j<=size;++j)
		{
			X[i][k] += P[i][j] * Y[j][k];
		}
	}
}


float **Ax = matrix(1,size,1,size);
//
for(int i=1;i<=size;++i)
{
	for(int k=1;k<=size;++k)
	{
		Ax[i][k]=0.0;
		for(int j=1;j<=size;++j)
		{
			Ax[i][k] += A[i][j] * X[j][k];
		}
	}
}
//

FILE *output=fopen("X.dat","w");

for(int i=1;i<=size;i++){
	for(int j=1;j<=size;++j)
	{
		fprintf(output,"%f\t",X[i][j]);
	}
	fprintf(output,"\n");

}

fclose(output);


FILE *output2=fopen("lambda.dat","w");

for(int i=1;i<=size;i++){
	fprintf(output2,"%f \n",d[i]);

}

fclose(output2);



float* b = vector(1,size);

for(int i=1;i<=size;++i)
{
	b[i]=0;
	for(int j=1;j<=size;++j)
	{
		b[i]+=X[j][i]*Ax[j][i];
	}
}


FILE *output3=fopen("beta.dat","w");

for(int i=1;i<=size;i++){
	fprintf(output3,"%f \n",b[i]);

}

fclose(output3);

return 1;
}