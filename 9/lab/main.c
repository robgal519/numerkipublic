#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"
#include <stdlib.h>

const float o = 4;
const float x_0 = 2;
float d(float x)
{
	return 0.1*(rand()/(RAND_MAX +1.0)-0.5);
}

float foo(double x)
{

	//return exp(-pow((x-x_0),2)/(2*o*o));

	return exp(-pow((x-x_0),2)/(2*o*o))*(1+d(x));

}

int m=4;

float F_x(float x, float** b)
{
	return b[1][1]+b[2][1]*x+b[3][1]*x*x+b[4][1]*x*x*x;

}

int main()
{
const int rozdzielczosc = 200;



const float x_max=3 * o + x_0;
const float x_min=-3 * o + x_0;

const int n = 11;

float skok = (x_max - x_min)/(n-1);


	float *X = vector(1,n);
	float *F = vector(1,n);

	float ** G = matrix(1,m,1,m);

	for(int i=1;i<=n;++i)
	{		
		X[i]=x_min+(i-1)*skok;
		F[i]=log(foo(X[i]));
	}

	for(int i=1;i<=m;++i)
	{
		for(int k=1;k<=m;++k)
		{
			G[i][k] = 0.0;
			for(int j=1;j<=n;++j)
			{
				G[i][k] +=pow(X[j], i+k-2);
			}
		}
	}

	float **r =matrix(1,m,1,1);
	for(int i=1;i<=m;++i)
	{
		r[i][1]=0.0;
		for(int j=1;j<=n;++j)
			r[i][1] += F[j]*pow(X[j],i-1);
	}

	gaussj(G,m,r,1);



	float skok_2 = (x_max-x_min)/rozdzielczosc;
	char nazwa1[20];
	sprintf(nazwa1,"G2-F1_%d.dat",n);
	FILE * f1 = fopen(nazwa1,"w");
	for(int i=1;i<=n;++i)
	{
		

		fprintf(f1,"%f\t%f\n",X[i],foo(X[i]));
	}


	char nazwa2[20];
	sprintf(nazwa2,"G2-F2_%d.dat",n);
	FILE * f2 = fopen(nazwa2,"w");
	for(int i=0;i<rozdzielczosc;++i)
		{
			float arg = x_min + skok_2*i;
		fprintf(f2,"%f\t%f\n",arg,exp(F_x(arg,r)));
	}

fclose(f1);
fclose(f2);

char nazwa3[20];
	sprintf(nazwa3,"G2-baza_%d.dat",n);
FILE* f3 = fopen(nazwa3,"w");

float a[4];
a[0]=(-x_0*x_0/2)/(o*o);
a[1]=x_0/(o*o);
a[2] = -0.5/(o*o);
a[3]=0;
for(int i=0;i<4;++i)
	fprintf(f3,"x^%d\t%f\t%f\n",i,a[i],r[i+1][1]);

fclose(f3);
}
