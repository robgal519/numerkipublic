#include "../numerical_recipes.c/nr.h"
#include "../numerical_recipes.c/nrutil.h"
#include "../numerical_recipes.c/nrutil.c"
#include "../numerical_recipes.c/gaussj.c"
#include "../numerical_recipes.c/tqli.c"
#include "../numerical_recipes.c/tred2.c"
#include "../numerical_recipes.c/pythag.c"
#include "math.h"
#include "stdio.h"
double f(double x)
{
	return pow((log(x)-x),6)-1;
}

double dF(double x)
{
	return 6*pow((log(x)-x),5)*((1/x)-1);
}
//////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!
double foo2(double x)
{
	return x*x*x + 2*x*x-3*x+4;
}

double dFoo2(double x)
{
	return 3*x*x+4*x-3;
}





int main()
{
	FILE *pl = fopen("results.dat","w");
	double x_0 = 3.01;
	double x_1 = 3.0;
	double x_2;
	double x_d = 1.0;
	double ep_0= fabs(x_1 - x_d);
	double ep_1 = ep_0;
	double ep_2 = ep_0;

	double p;
	double C;

fprintf(pl,"it\tf(x_i)\t\t\tx_2\t\t\tep\t\t\tp\nmetoda siecznych \n-------------------------------------------------------------------------------------\n");
	for(int i=0;i<20;++i)
	{
		double fu = f(x_1);
		x_2 = x_1 - fu*(x_1-x_0)/(fu-f(x_0));

		x_0 = x_1;
		x_1 = x_2;
		ep_0 = ep_1;
		ep_1 = ep_2;
		ep_2=fabs(x_1 - x_d);

		p=(log(ep_1/ep_2)/log(ep_0/ep_1));

		C = ep_2/pow(ep_1,p);

		fprintf(pl,"%d\t%.15f\t%.15f\t%.15f\t%.15f\n",i,fu,x_2,ep_2,p);

	}

	fprintf(pl,"//////////////////////////////////////////////\nmetoda Newtona \n");

	x_0 = 3.0;

	for(int i=0;i<20;++i)
	{
		double fu = f(x_0);
		x_1 = x_0 - fu/dF(x_0);
		x_0 = x_1;

		ep_0 = ep_1;
		ep_1 = ep_2;
		ep_2=fabs(x_1 - x_d);

		p=(log(ep_1/ep_2)/log(ep_0/ep_1));

		C = ep_2/pow(ep_1,p);

		fprintf(pl,"%d\t%.15f\t%.15f\t%.15f\t%.15f\n",i,fu,x_1,ep_2,p);


	}


////////////////////////////////////////////////////

	x_0 = -20.1;
	x_1 = -20;
	x_d = -3.284277537306950;
	ep_0= fabs(x_1 - x_d);
	ep_1 = ep_0;
	ep_2 = ep_0;

	fprintf(pl,"it\tf(x_i)\t\t\tx_2\t\t\tep\t\t\tp\nmetoda siecznych \n-------------------------------------------------------------------------------------\n");
	for(int i=0;i<20;++i)
	{
		double fu = foo2(x_1);
		x_2 = x_1 - fu*(x_1-x_0)/(fu-foo2(x_0));

		x_0 = x_1;
		x_1 = x_2;
		ep_0 = ep_1;
		ep_1 = ep_2;
		ep_2=fabs(x_1 - x_d);

		p=(log(ep_1/ep_2)/log(ep_0/ep_1));

		C = ep_2/pow(ep_1,p);

		fprintf(pl,"%d\t%.15f\t%.15f\t%.15f\t%.15f\n",i,fu,x_2,ep_2,p);

	}

	fprintf(pl,"//////////////////////////////////////////////\nmetoda Newtona \n");

	x_0 = -20;

	for(int i=0;i<20;++i)
	{
		double fu = foo2(x_0);
		x_1 = x_0 - fu/dFoo2(x_0);
		x_0 = x_1;

		ep_0 = ep_1;
		ep_1 = ep_2;
		ep_2=fabs(x_1 - x_d);

		p=(log(ep_1/ep_2)/log(ep_0/ep_1));

		C = ep_2/pow(ep_1,p);

		fprintf(pl,"%d\t%.15f\t%.15f\t%.15f\t%.15f\n",i,fu,x_1,ep_2,p);


	}
fclose(pl);

	return 0;
}