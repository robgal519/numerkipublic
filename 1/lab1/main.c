#include "numerical_recipes.c/nr.h"
#include "numerical_recipes.c/nrutil.h"
#include "numerical_recipes.c/nrutil.c"
#include "numerical_recipes.c/gaussj.c"
#include "math.h"
#include "stdio.h"

int main(){


int size=200;
float** a;
float** b;
float A=1;
float h=0.1;
float v=0;
float w = 1;

a = matrix(1,size,1,size);
b = matrix(1,size,1,1);

for(int i=1;i<=size;++i)
	for(int j=1;j<=size;++j)
	{
		a[i][j]=0.0;
		if(i==j)
			a[i][j]=1.0;
		if(i>=3){
			a[i][i-1]=w*w*h*h-2.0;
			a[i][i-2]=1.0;
	}
	}

a[2][1]=-1.0;

for(int i=1;i<=size;++i)
	b[i][1]=0.0;
b[1][1]= A;
b[2][1] = h*v;


gaussj(a,size,b,1);



FILE *output=fopen("out.data","w");
float t, dok;

for(int i=1;i<=size;i++){
	t=h*(i-1);
	dok = A* cos(w*t);
	fprintf(output,"%f\t%f\t%f\n", t,b[i][1],dok);
}

fclose(output);
	return 0;
}