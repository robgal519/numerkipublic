#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "../../numerical_recipes.c/sinft.c"
#include "../../numerical_recipes.c/realft.c"
#include "../../numerical_recipes.c/four1.c"
#include "../../numerical_recipes.c/gauleg.c"
#include "../../numerical_recipes.c/gaulag.c"
#include "../../numerical_recipes.c/gammln.c"
#include "../../numerical_recipes.c/gauher.c"
#include "math.h"
#include "stdio.h"
#include <stdlib.h>
#include <time.h>

float gXY (float x, float y);
float f (float x);
float Rnd();
float CX();


int main(){
	int N = 1e5;

	float I_dok = 0.2557840245;
	float I_an = 0.0;
	float I_kw = 0.0;
	srand(time(NULL));

	FILE * p1 = fopen("C1.dat","w");

	for(int n=10;n<=N;n*=10){
		I_an = 0.0;
		I_kw = 0.0;
		for(int j=1;j<=n;j++){
			float x = Rnd();
			float y = Rnd();
			I_an += gXY(x,y)*f(x)*f(y);
			I_kw +=gXY(x,y)*f(x)*f(y)*gXY(x,y)*f(x)*f(y);
		}

		float delta = (I_kw - (I_an* I_an)/n)/(n-1.0);
		float I_an_ = I_an/n;
		float odch = sqrt(delta)/sqrt(n);
		fprintf(p1,"%f\t%f\t%f\n",I_an_,delta,odch);
		
	}
	FILE * p2 = fopen ("C2.dat","w");
	for(int n=10;n<=N;n*=10){
		I_an = 0.0;
		I_kw = 0.0;
		for(int j=1;j<=n;j++){
			float x = -log(1-Rnd()/CX());
			float y = -log(1-Rnd()/CX());
			I_an += gXY(x,y)/(CX()*CX());
			I_kw +=(gXY(x,y)/(CX()*CX()))*(gXY(x,y)/(CX()*CX()));
		}

		float delta = (I_kw - (I_an* I_an)/n)/(n-1.0);
		float I_an_ = I_an/n;
		float odch = sqrt(delta)/sqrt(n);

		fprintf(p2,"%f\t%f\t%f\n",I_an_,delta,odch);
		
	}

	const int k = 10;

	float skok = 1.0/k;

	int tab[10]={0};
	for(int i=0;i<1e5;i++){
		float x =  -log(1-Rnd()/CX());
		tab[(int)((x/skok))]++;
	}
	float T [10];
	for(int i=0;i<10;++i)
	{
		T[i] = tab[i]/1e5;
	}
	// int sum=0;
	// printf("\n%d\n",sum);
FILE * p3 = fopen ("C3.dat","w");
float P[10]={0};
	for(int i=1;i<=10;i++){
		 P[i-1] = CX()*(1-exp(-i*skok)) - CX()*(1-exp(-(i-1)*skok));
		fprintf(p3,"%f\t%f\t%f\n",i*skok,T[i-1],P[i-1]);
	}
	FILE * p4 = fopen("C4.dat","w");
float chi_kw = 0;
for(int i=0;i<k;++i)
{
	chi_kw += pow((tab[i] - N*P[i]),2)/(1.0*N*P[i]);
}
fprintf(p4,"%f",chi_kw);


	
	// printf("%f\t%f\t%f\n",I_an,delta,odch);



return 0;
	
}


////////////////////////////////////////
float CX(){
	return 1.0/(1-exp(-1));
}

float Rnd(){
	return rand()/(RAND_MAX + 1.0);
}
float gXY(float x, float y){
	return sin(x+y)/log(2+x+y);
}
float f(float x){
	return exp(-x);
}