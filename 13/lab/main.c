// #include "../../numerical_recipes.c/nr.h"
// #include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
// #include "../../numerical_recipes.c/gaussj.c"
// #include "../../numerical_recipes.c/tqli.c"
// #include "../../numerical_recipes.c/tred2.c"
// #include "../../numerical_recipes.c/pythag.c"
// #include "../../numerical_recipes.c/sinft.c"
// #include "../../numerical_recipes.c/realft.c"
// #include "../../numerical_recipes.c/four1.c"
#include "../../numerical_recipes.c/gauleg.c"
#include "../../numerical_recipes.c/gaulag.c"
#include "../../numerical_recipes.c/gammln.c"
#include "../../numerical_recipes.c/gauher.c"
#include "math.h"
#include "stdio.h"
#include <stdlib.h>
#include <time.h>

float gau(float *A, float *X, int n, float (*foo)(float));
float f1(float arg);
float f2(float arg);
float f3(float arg);

#define ABS(x) (((x)<0)?(-(x)):(x))

int main(){
float precise1 = 4*atan(1)/3.0;
float precise2 = -0.8700577;
float precise3 = 2.0/13.0;
	float * x = vector(1,100);
	float * w = vector(1,100);
	FILE * plik1 = fopen("1.dat","w");
	FILE * plik2 = fopen("2a.dat","w");
	FILE * plik3 = fopen("2b.dat","w");
	FILE * plik4 = fopen("3.dat","w");

	for(int i=2;i<=100;i++){
		gauleg(1,2,x,w,i);
		fprintf(plik1,"%d\t%f\n",i,ABS(gau(w,x,i,f1) - precise1));
	}
	printf("\n\n");
	for(int i=2;i<=100;i+=2){
		gauher(x,w,i);
		fprintf(plik2,"%d\t%f\n",i,ABS(gau(w,x,i,f2)/2.0 - precise2));
	}
	printf("\n\n");
	for(int i=2;i<=100;i+=1){
		gauleg(0,5,x,w,i);
		fprintf(plik3,"%d\t%f\n",i,ABS(gau(w,x,i,f2) - precise2));
	}
	printf("\n\n");
	for(int i=2;i<=10;i+=1){
		gaulag(x,w,i,0);
		fprintf(plik4,"%d\t%f\n",i,ABS(gau(w,x,i,f3) - precise3));
	}

fclose(plik1);
fclose(plik2);
fclose(plik3);
fclose(plik4);

return 0;
	
}


////////////////////////////////////////

float f1(float arg){
	return (1.0)/(arg*sqrt(arg*arg-1));
}
float f2(float arg){
	return log(ABS(arg))*exp(-arg*arg);
}
float f3(float arg){
	return sin(2*arg)*exp(-2*arg);
}

float gau(float *A, float *X, int n, float (*foo)(float)){
	float sum=0;

	for(int i=1;i<=n;i++)
		sum+=A[i]*foo(X[i]);

	return sum;
}