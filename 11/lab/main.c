#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "../../numerical_recipes.c/sinft.c"
#include "../../numerical_recipes.c/realft.c"
#include "../../numerical_recipes.c/four1.c"
#include "math.h"
#include "stdio.h"
#include <stdlib.h>
#include <time.h>

const int k = 10;

float frand(); // generator pseudolosowy z zakresu [0 : 1]

int sign(); // funkcja zwracajaca 1, lub -1, z prawdopodobienstwem 50%

float max(float * tab, int size); // funkcja zwraca indeks pod krurym znajduje sie maximum z tablicy tab, size, to rozmiar tablicy, indeksowanej od 1

#define ABS(x) (((x)<0 )? (-(x)) : (x))

#define M_PI  (4*atan(1))


int main(){

	srand(time(NULL));

	const int n = 1 << k; 
		printf("%d",n);
	const float omega = 4.0*M_PI/n;

	char nazwa_pliku[] = "w_k10.dat";
	FILE* plik = fopen(nazwa_pliku,"w");

	float *y_0 = vector(1,n);
	float *y_szum = vector(1,n);
	float *y = vector(1,n);
	
	for(int i=1;i<=n;++i)
	{
		y_0[i] = sin(omega*i) + sin(2*omega*i) + sin(3*omega*i);

		float a = (2*sign()*frand()); // modul wprowadzqajacy szum
		y[i] = y_0[i] + a;
		y_szum[i] = y[i];
	}

	sinft(y,n);

	float M = max(y,n);
	

	float *FU = vector(1,n);

	for(int i=1;i<=n;++i){
		FU[i]=y[i];

		if(ABS(y[i])<M/4.0)
			y[i]=0;
		
		y[i]=y[i]*2/n;
		
	}

	sinft(y,n);

	for(int i=1;i<=n;++i)
		fprintf(plik,"%d\t%f\t%f\t%f\t%f\n",i,y_0[i],y_szum[i],y[i],FU[i]);

	fclose(plik);
return 0;
}


////////////////////////////////////////
float frand()
{
	return rand()/(RAND_MAX + 1.0);
}

int sign(){
	float Y = frand();
	return Y>0.5 ? 1 : -1;
}

float max(float * tab, int size){
	int max = 1;
	for(int i=2;i<=size;++i)
		if(tab[i]>tab[max])
			max = i;
	return tab[max];
}