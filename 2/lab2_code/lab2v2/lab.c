#include <stdio.h>

int main(int argc, char const *argv[])
{
	const int N=500;
	const float a=0.5;
	const float b=2.0;
	const float h=2*b/(N-1);

	float X[N+1];
	float P[N+1];

	for (int i = 1; i <= N; ++i)
	{
		X[i]=-b+h*(i-1)	;
		P[i]=0;
	}

//odrazu jest -P
	for (int i = 1; i <=N; ++i)
	{
		if(X[i]>=-a && X[i] < 0)
			P[i]=-1;
		if(X[i]>0 && X[i]<=a)
			P[i]=1;
	}

////////////////////////
	float D[N+1];
	float A[N+1];
	float C[N+1];

	for (int i = 1; i <=N; ++i)
	{
		D[i]=-2.0/(h*h);
		A[i]=C[i]=1.0/(h*h);
	}
////////////////////////
	D[1]=D[N]=1;
	C[1]=P[1]=A[N]=P[N]=0;

////////////////////////

	float U[N+1];
	float L[N+1];

////////////////////////
	U[1]=D[1];
	for(int i=2;i<=N;++i)
	{
		L[i]=A[i]/U[i-1];
		U[i]=D[i]-L[i]*C[i-1];
	}

////////////////////////

	float Y[N+1];
	float V[N+1];

////////////////////////
	Y[1]=P[1];
	for (int i = 2; i <=N; ++i)
	{
		Y[i]=P[i]-L[i]*Y[i-1];
	}

	V[N]=Y[N]/U[N];
	for (int i = N-1; i >= 1; --i)
	{
		V[i]=(Y[i]-C[i]*V[i+1])/U[i];
	}

//////////////////////////////

	float analityczne[N+1];

	for (int i = 1; i <=N; ++i)
	{
		if(X[i]<=-a)
		{
			analityczne[i]=X[i]*1.0/16 + 1.0/8;
		}else
		if(X[i]<=0)
		{
			analityczne[i]=X[i]*X[i]*(-1.0)/2 -  X[i] * 7.0/16;
		}else
		if(X[i]<=a)
		{
			analityczne[i]=X[i]*X[i]*(1.0)/2 -  X[i] * 7.0/16;
		}else
		if(X[i]>a)
		{
			analityczne[i]=X[i]*1.0/16 - 1.0/8;
		}
	}


FILE *output = fopen("out.dat","w");

for (int i = 1; i <= N; ++i)
{
	fprintf(output,"%f\t%f\t%f\n",X[i],V[i],analityczne[i]);
	printf("%f\t%f\t%f\n",X[i],V[i],analityczne[i]);
}
fclose(output);
	return 0;
}
