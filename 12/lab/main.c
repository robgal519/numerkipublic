#include "../../numerical_recipes.c/nr.h"
#include "../../numerical_recipes.c/nrutil.h"
#include "../../numerical_recipes.c/nrutil.c"
#include "../../numerical_recipes.c/gaussj.c"
#include "../../numerical_recipes.c/tqli.c"
#include "../../numerical_recipes.c/tred2.c"
#include "../../numerical_recipes.c/pythag.c"
#include "../../numerical_recipes.c/sinft.c"
#include "../../numerical_recipes.c/realft.c"
#include "../../numerical_recipes.c/four1.c"
#include "math.h"
#include "stdio.h"
#include <stdlib.h>
#include <time.h>

const int n = 8;
 int a=0; //granica calkowania
 int b=1;

float milne(int a,int b, int i);
float sim(int a,int b, int i);
float ekstrapolacja(float **D, int k, int n);
float f(float x);


int main(){

	float **DMilne = matrix(0,n+1,0,n+1);
	float **DSim = matrix(0,n+1,0,n+1);

	for(int i=0;i<=n;i++)
	{
		DMilne[i][0]=milne(a,b,i);
		DSim[i][0]=sim(a,b,i);
	}

	for(int k=1;k<=n; k++)
	{
		for(int m=k;m<=n;m++)
		{
			DMilne[m][k] = ekstrapolacja(DMilne, k, m);
			DSim[m][k] = ekstrapolacja(DSim, k, m);
		}
	}
	FILE* plik = fopen("data.dat","w");

fprintf(plik,"milne:\n");
for(int k=0;k<=n; k++)
	{
		for(int m=0;m<=k;m++)
		{
			fprintf(plik,"%f\t",DMilne[k][m]);
			
		}
		fprintf(plik,"\n");
	}

fprintf(plik,"Sim:\n");
for(int k=0;k<=n; k++)
	{
		for(int m=0;m<=k;m++)
		{
			fprintf(plik,"%f\t",DSim[k][m]);
			
		}
		fprintf(plik,"\n");
	}
return 0;
	
}


////////////////////////////////////////
float milne(int a, int b, int i){
	int pow=1<<(i+2);
	
 	float h=(b-a)/(float)pow;
 	float Sum =0.0;
 	for(int j=0 ; j<=pow/4-1; j++)
 		Sum += (4*h/90.0) * ( 7*f((a+h*(4*j))) + 32 * f(a+h*(4*j+1)) + 12 * f(a+h*(4*j+2)) + 32 * f(a+h*(4*j+3)) + 7 * f(a+h*(4*j+4)));
 return Sum;
}

float sim(int a, int b, int i){
	int pow=1<<(i+1);
	
	float h=(b-a)/(float)pow;

	float Sum =0.0;
 	for(int j=0;j<=pow/2-1;++j)
 		Sum += (h/3.0) * ( f(a+h*(2*j)) + 4*f(a+h*(2*j+1)) + f(a+h*(2*j+2)) );
 	return Sum;
}

float f(float x){
	return log(x*x*x + 3*x*x+ x +0.1)*sin(18*x);
}
float ekstrapolacja(float **D, int k, int n){
	return (float)pow(4,k)/(pow(4,k)-1) * D[n][k-1] - 1.0/(pow(4,k)-1) * D[n-1][k-1];

}
