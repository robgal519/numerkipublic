#include <stdio.h>
#include <math.h>

#define max(X,Y) ((X)>(Y)? (X):(Y))
#define min(X,Y) ((X)<(Y)? (X):(Y))
#define abs(X) ((X)>0? (X):-(X))

int main(){

int n = 1000;
int m=5;

//////////////////////////
double A[n+1][n+1];

for(int i=1;i<=n;++i)
{
	for(int j=1;j<=n;++j)
	{
		if(abs(i-j)<=m)
			A[i][j]=1.0/(1+abs(i-j));
		else
			A[i][j] = 0;
	}
}
///////////////////////

double b[n+1];

for(int i = 1; i<=n;++i)
{
	b[i]=i;
}
//////////////////////////

double x[n+1];
for(int i=0;i<=n;++i)
	x[i]=0;

double rr = 1.0;
int it = 0;
double Ax[n+1];

double r[n+1];

double z[n+1];

FILE *output=fopen("out.dat","w");


while(rr>10.0e-6 && it <= 500)
{
	it++;
//Ax
for(int i = 1;i<=n;++i)
{
	int jmin = max(1,i-m);
	int jmax = min(n,i+m);
	Ax[i]=0.0;
	for(int j=jmin;j<=jmax;++j)
	{
		Ax[i]+=A[i][j]*x[j];
	}
}
//r
for(int i=1;i<=n;++i){
	r[i]=b[i]-Ax[i];
}

double licznik =0;
for(int i=1;i<=n;++i)
{
	licznik+=r[i]*r[i];
}
//Z
for(int i = 1;i<=n;++i)
{
	int jmin = max(1,i-m);
	int jmax = min(n,i+m);
	z[i]=0.0;
	for(int j=jmin;j<=jmax;++j)
	{
		z[i]+=A[i][j]*r[j];
	}
}


double mianownik = 0;
for(int i=1;i<=n;++i){
	mianownik+=r[i]*z[i];
}

double alpha = licznik/mianownik;
//x
for(int i=1;i<=n;++i)
{
	x[i]+= alpha*r[i];
}

rr=sqrt(licznik);
double xx = 0;
for(int i=1;i<=n;++i)
{
	xx+=x[i]*x[i];
}

xx= sqrt(xx);


printf("%d\t%f\t%f\n",it,rr,xx);

fprintf(output,"%d\t%f\t%f\n",it,rr,xx);
}

fclose(output);
}

/*

mozna  wrcic na float, wtedy wywalana liczniku petli

nalezy stwierdzic, ze wystepuja bledy numeryczne i nie mozna dojsc do ostatecznego wyniku ale ten zblizyl sie dosc blisko

*/
